'use strict';

module.exports = function(req, res, next) {

    let token = req.headers['authorization'];

    if (!token) {
        res.status(401).send({ mensagem: 'Não autorizado' });
        return;
    }

    req.token = token;

    next();
};
