'use strict';

const crypto = require('crypto');

module.exports = (value) => {
    return crypto.pbkdf2Sync(value, 'salt', 1000, 256, 'SHA512').toString('hex');
};
