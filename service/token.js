'use strict';

const config = require('../config/index.json');
const jwt = require('jsonwebtoken');

module.exports = function(values) {
    return jwt.sign(values, config.key);
};
