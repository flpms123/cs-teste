'use strict';

const crypt = require('./crypt.js');
const token = require('./token.js');
const validateToken = require('./validate-token.js');

module.exports = {
    crypt: crypt,
    token: token,
    validateToken: validateToken
};
