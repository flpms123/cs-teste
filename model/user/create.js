'use strict';

const DB = require('simple-connection');
const uuid = require('node-uuid');
const service = require('../../service/');

let User = function(user) {

    let config = this.config;
    let db = DB(config.db[global.environment]);
    let collection = db.collection('users');

    let promise = new Promise((resolve, reject) => {

        let ddInvalid = null;
        let phoneInvalid = null;

        user.telefones.forEach(phone => {
            ddInvalid = !phone.ddd || !(/[\d]/gi).test(phone.ddd);
            phoneInvalid = !phone.numero || !(/[\d]{8,9}/gi).test(phone.numero);
        });

        if (ddInvalid || phoneInvalid) {
            return reject({statusCode: 400, message: 'o campo telefones precisa ter um numero com ddd valido'});
        }

        let token = service.token({ email: user.email, senha: user.senha });

        let date = new Date();

        /*eslint-disable*/
        user._id = uuid.v4();
        user.data_criacao = date;
        user.data_atualizacao = date;
        user.ultimo_login = date;
        /*eslint-disable*/
        user.token = service.crypt(token);
        user.senha = service.crypt(user.senha);

        return collection.insert(user).then(result => resolve(result)).catch(err => reject(err));
    });

    return promise;
};

module.exports = User;
