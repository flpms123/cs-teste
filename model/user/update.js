'use strict';

const DB = require('simple-connection');
const jwt = require('jsonwebtoken');
const service = require('../../service/');

let User = function(user) {

    /* eslint max-statements: ["error", 23] */

    let db = DB(this.config.db[global.environment]);
    let collection = db.collection('users');

    let promise = new Promise((resolve, reject) => {

        if (!user) {
            return reject({statusCode: 400, message: 'usuario nao informado'});
        }

        let userId = user.email || user.id;

        if (!userId) {
            return reject({ statusCode: 400, message: 'sem referencia para alteracao, informe o email ou id' });
        }

        if (!user.hasOwnProperty('$set') && !user.token) {
            return reject({ statusCode: 400, message: 'alteracao nao permitida'});
        }

        if (user.$set && user.$set.senha) {
            user.$set.senha = service.crypt(user.$set.senha);
        }

        if (user.token && (!user.email || !user.senha)) {
            return reject({ statusCode: 400, message: 'alteracao nao permitida email e/ou senha nao informado'});
        }

        if (user.token) {
            let token = jwt.sign({ email: user.email, senha: user.senha }, this.config.key);

            /* eslint-disable */
            delete user.senha;
            delete user.token;
            /* eslint-disable */
            user.$set = { token: service.crypt(token)};
        }

        let selector = {};

        if (user.email) {
            /* eslint-disable */
            selector.email = user.email;
            delete user.email;
            /* eslint-disable */
        }

        if (user.id) {
            /* eslint-disable */
            selector._id = user.id;
            delete user.id;
            /* eslint-disable */
        }

        let op =  collection.update(selector, user);
        op.then(result => resolve(result.result))
        op.catch(err => reject({ statusCode: 503, message: err.message }));

        return undefined;
    });

    return promise;
};

module.exports = User;
