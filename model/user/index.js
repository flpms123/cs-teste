'use strict';

const create = require('./create.js');
const find = require('./find.js');
const update = require('./update.js');
const config = require('../../config/index.json');

module.exports = {
    create(user) {
        return create.call({ context: this, config: config }, user);
    },
    find(user) {
        return find.call({ context: this, config: config }, user);
    },
    update(user) {
        return update.call({ context: this, config: config }, user);
    }
};
