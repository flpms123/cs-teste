'use strict';

const DB = require('simple-connection');

let find = function(user) {

    let db = DB(this.config.db[global.environment]);
    let collection = db.collection('users');

    let promise = new Promise((resolve, reject) => {

        if (!user) {
            return reject({ statusCode: 400, message: 'sem dados para pesquisa'});
        }

        if (typeof user !== 'object' && !Array.isArray(user)) {
            return reject({ statusCode: 400, message: 'precisa ser um objeto para pesquisa'});
        }

        let colUser = collection.find(user).then(result => resolve(result))

        colUser.catch(err => reject(err));
        return colUser;
    });

    return promise;
};

module.exports = find;
