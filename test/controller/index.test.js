'use strict';

const chai = require('chai');
const assert = require('assert');
const index = require('../../controller');
const expect = chai.expect;

describe('Test controller list', function() {
    it ('Test index user object', function() {
        expect(index).to.be.a('object');
    });

    it('Test index has user controller', function(){
        expect(index).to.have.property('user');
    });

    it('Test index has auth controller', function(){
        expect(index).to.have.property('auth');
    });
});
