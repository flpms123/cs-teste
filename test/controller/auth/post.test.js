'use strict';

const chai = require('chai');
const assert = require('assert');
const DB = require('simple-connection');

const auth = require('../../../controller/auth/');
const expect = chai.expect;

describe('Controller auth test', function() {
    before(function(done) {

        global.environment = 'test';

        const uuid = require('node-uuid');
        const jwt = require('jsonwebtoken');
        const service = require('../../../service/');

        var collection, db, date, token, user;

        date = new Date();

        user = {
            nome: 'Filipe',
            email: 'filipe@filipe.com',
            senha: 'Senha123Forte',
            telefones: [{ numero: "123456789", ddd: "11" }],
            _id: uuid.v4(),
            data_criacao: date,
            data_atualizacao: date,
            ultimo_login: date,
            senha: service.crypt('Senha123Forte')
        };

        token = jwt.sign({email: user.email, nome: user.senha}, "d208eed1c1d61bbdd579fa0cf4ab9e81b9b6eaa6770480d4d616cda5b9ca6b70e32c2d273bb45d93fbecdb3b09a28b00f90e85136752fcaf2338a19d68653b24");
        user.token = service.crypt(token);

        db = DB({
            "username": "",
            "password": "#",
            "server": "localhost",
            "port": 27017,
            "database_name": "test-auth-user"
        });

        collection = db.collection('users');
        collection.insert(user).then(function(result) {
            done();
        }).catch(function(err) {
            done(err);
        });
    });

    it ('sucess when call auth creation', function() {
        expect(auth).to.be.a('object');
        expect(auth).to.have.property('create');
        expect(auth.create).to.be.a('function');
    });

    it('expect failure cause body not informed', function(done) {
        var res, req;
        req = {};

        res = {
            status(statusCode) {
                this.statusCode = statusCode;
                return this;
            },
            send(result) {

                expect(result).to.be.a('object');
                expect(result).to.have.property('mensagem');
                expect(result.mensagem).to.be.a('string');
                expect(result.mensagem).to.equal('solicitacao invalida');

                expect(this.statusCode).to.be.a('number');
                expect(this.statusCode).to.equal(400);

                done();
            }
        };

        auth.create(req, res);
    });

    it('expect failure cause email not informed', function(done) {
        var res, req;
        req = {
            body: {
                senha: '123'
            }
        };

        res = {
            status(statusCode) {
                this.statusCode = statusCode;
                return this;
            },
            send(result) {

                expect(result).to.be.a('object');
                expect(result).to.have.property('mensagem');
                expect(result.mensagem).to.be.a('string');
                expect(result.mensagem).to.equal('usuario e/ou senha nao informado');

                expect(this.statusCode).to.be.a('number');
                expect(this.statusCode).to.equal(400);

                done();
            }
        };

        auth.create(req, res);
    });

    it('expect failure cause pass not informed', function(done) {
        var res, req;
        req = {
            body: {
                email: 'filipe@filipe.com'
            }
        };

        res = {
            status(statusCode) {
                this.statusCode = statusCode;
                return this;
            },
            send(result) {

                expect(result).to.be.a('object');
                expect(result).to.have.property('mensagem');
                expect(result.mensagem).to.be.a('string');
                expect(result.mensagem).to.equal('usuario e/ou senha nao informado');

                expect(this.statusCode).to.be.a('number');
                expect(this.statusCode).to.equal(400);

                done();
            }
        };

        auth.create(req, res);
    });

    it('expected failure cause user don\'t exist', function(done) {

        var res, req;
        req = {
            body: {
                email: 'filipe.silva@filipe.com',
                senha: '123456'
            }
        };

        res = {
            status(statusCode) {
                this.statusCode = statusCode;
                return this;
            },
            send(result) {

                expect(result).to.be.a('object');
                expect(result).to.have.property('mensagem');
                expect(result.mensagem).to.be.a('string');
                expect(result.mensagem).to.equal('Usuário e/ou senha inválidos');

                expect(this.statusCode).to.be.a('number');
                expect(this.statusCode).to.equal(401);

                done();
            }
        }

        auth.create(req, res);
    });

    it('expected failure cause password is wrong', function() {

        var res, req;
        req = {
            body: {
                email: 'filipe@filipe.com',
                senha: 'Senha125Forte'
            }
        };

        res = {
            status(statusCode) {
                this.statusCode = statusCode;
                return this;
            },
            send(result) {

                expect(result).to.be.a('object');
                expect(result).to.have.property('mensagem');
                expect(result.mensagem).to.be.a('string');
                expect(result.mensagem).to.equal('Usuário e/ou senha inválidos');

                expect(this.statusCode).to.be.a('number');
                expect(this.statusCode).to.equal(401);

                done();
            }
        }

        auth.create(req, res);
    });

    it('expected sucess cause all data is informed correctly', function(done) {

        var res, req;

        req = {
            body: {
                email: 'filipe@filipe.com',
                senha: 'Senha123Forte'
            }
        };

        res = {
            status(statusCode) {
                this.statusCode = statusCode;
                return this;
            },
            send(result) {

                expect(result).to.be.a('object');
                expect(result).to.have.property('nome');
                expect(result).to.have.property('id');
                expect(result).to.have.property('email');
                expect(result).to.have.property('telefones');
                expect(result).to.have.property('data_criacao');
                expect(result).to.have.property('data_atualizacao');
                expect(result).to.have.property('ultimo_login');
                expect(result).to.have.property('token');

                expect(result.nome).to.be.a('string');
                expect(result.id).to.be.a('string');
                expect(result.telefones).to.be.a('array');
                expect(result.email).to.be.a('string');
                expect(result.data_criacao).to.be.a('date');
                expect(result.ultimo_login).to.be.a('date');
                expect(result.data_atualizacao).to.be.a('date');

                expect(result.email).to.equal('filipe@filipe.com');
                expect(result.nome).to.equal('Filipe');
                expect(this.statusCode).to.be.a('number');
                expect(this.statusCode).to.equal(200);

                done();
            }
        };

        auth.create(req, res);
    });

    after(function(done){

        let db, collection;

        db = DB({
            "username": "",
            "password": "#",
            "server": "localhost",
            "port": 27017,
            "database_name": "test-auth-user"
        });

        collection = db.collection('users');
        collection.remove({}).then(function() {
            done();
        }).catch(function(err) {
            done(err);
        });
    });
});
