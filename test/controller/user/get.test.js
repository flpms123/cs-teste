'use strict';

const chai = require('chai');
const assert = require('assert');
const DB = require('simple-connection');

const service = require('../../../service/');
const user = require('../../../controller/user/');
const expect = chai.expect;

describe('Controller user test get', function() {

    var local = {};

    before(function(done) {

        global.environment = 'test';

        var collection, db;

        const uuid = require('node-uuid');

        var collection, db, date, token, user;
        local.uuid = uuid.v4();
        date = new Date();

        user = {
            nome: 'Filipe',
            email: 'filipe@filipe.com',
            senha: 'Senha123Forte',
            telefones: [{ numero: "123456789", ddd: "11" }],
            _id: local.uuid,
            data_criacao: date,
            data_atualizacao: date,
            ultimo_login: date,
            senha: service.crypt('Senha123Forte')
        };

        local.token = service.token({ email: user.email, senha: user.senha });
        user.token = service.crypt(local.token);
        local.token = user.token;

        db = DB({
            "username": "",
            "password": "#",
            "server": "localhost",
            "port": 27017,
            "database_name": "test-auth-user"
        });

        collection = db.collection('users');
        collection.insert(user).then(function(result) {
            done();
        }).catch(function(err) {
            done(err);
        });
    });

    it('Expected a failure when find user, cause user_id not found', function(done) {

        var req, res;

        res = {
            status: function(statusCode) {
                this.statusCode = statusCode
                return this;
            },
            send: function(err) {
                expect(err).to.be.a('object');
                expect(this.statusCode).to.be.a('number');
                expect(this.statusCode).to.equal(400);

                expect(err.mensagem).to.equal('usuario nao informado');

                done();
            }
        };

        req = {
            params: {}
        };

        user.find(req, res);
    });

    it('Expected a failure when cause token user is not same', function(done) {

        this.timeout(5000);

        var req, res, token;

        res = {
            status(statusCode) {
                this.statusCode = statusCode
                return this;
            },
            send(err) {

                expect(err).to.be.a('object');
                expect(err).to.have.property('mensagem');
                expect(err.mensagem).to.equal('Não autorizado');
                expect(this.statusCode).to.be.a('number');
                expect(this.statusCode).to.equal(401);

                done();
            }
        };

        token = service.token({email: 'filipes@filipe.com', senha: '09887'});

        req = {
            token: 'Bearer ' + service.crypt(token),
            params: {
                id: local.uuid
            }
        };

        user.find(req, res);
    });

    it('Expected a sucess', function(done) {

        var req, res, token;

        res = {
            status(statusCode) {
                this.statusCode = statusCode
                return this;
            },
            send(result) {

                expect(result).to.be.a('object');
                expect(result).to.have.property('nome');
                expect(result).to.have.property('email');
                expect(result).to.have.property('data_criacao');
                expect(result).to.have.property('data_atualizacao');
                expect(result).to.have.property('ultimo_login');
                expect(result).to.have.property('token');
                expect(result.nome).to.equal('Filipe');
                expect(result.email).to.equal('filipe@filipe.com');
                expect(result.token).to.equal(local.token);

                expect(this.statusCode).to.be.a('number');
                expect(this.statusCode).to.equal(200);

                done();
            }
        };

        req = {
            token: 'Bearer ' + local.token,
            params: {
                id: local.uuid
            }
        };

        user.find(req, res);
    });

    after(function(done){
        var collection, db;

        db = DB({
            "username": "",
            "password": "#",
            "server": "localhost",
            "port": 27017,
            "database_name": "test-auth-user"
        });

        collection = db.collection('users');
        collection.remove({}).then(function() {
            done();
        }).catch(function(err) {
            done(err);
        });
    });
});
