'use strict';

const chai = require('chai');
const assert = require('assert');
const index = require('../../../controller/user');
const expect = chai.expect;

describe('Controller User test', function() {
    it ('Test index user object', function() {
        expect(index).to.be.a('object');
    });

    it('Test index has user create', function(){
        expect(index).to.have.property('create');
    });

    it('Test index has auth find', function(){
        expect(index).to.have.property('find');
    });
});
