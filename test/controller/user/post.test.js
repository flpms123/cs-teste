'use strict';

const chai = require('chai');
const assert = require('assert');
const DB = require('simple-connection');

const user = require('../../../controller/user/');
const expect = chai.expect;

describe('Controller user test', function() {
    before(function(done) {

        global.environment = 'test';

        var collection, db;

        db = DB({
            "username": "",
            "password": "#",
            "server": "localhost",
            "port": 27017,
            "database_name": "test-auth-user"
        });

        collection = db.collection('users');
        collection.remove({}).then(function() {
            done();
        }).catch(function(err) {
            done(err);
        });
    });

    it('Expected a failure when create user, cause post "empty" body', function(done) {
        var res = {
            status: function(statusCode) {
                this.statusCode = statusCode
                return this;
            },
            send: function(err) {
                expect(err).to.be.a('object');
                expect(this.statusCode).to.be.a('number');
                expect(this.statusCode).to.equal(400);

                expect(err.mensagem).to.equal('nenhum usuario informado');

                done();
            }
        };

        user.create({body: ''}, res);
    });

    it('Expected a failure when create user, cause missing "nome" parameters', function(done) {
        var res = {
            status: function(statusCode) {
                this.statusCode = statusCode;
                return this;
            },
            send: function(err) {

                expect(this.statusCode).to.be.a('number');
                expect(this.statusCode).to.equal(400);
                expect(err).to.be.a('object');
                expect(err).to.have.property('mensagem');
                expect(err.mensagem).to.equal('o campo nome é obrigatório');

                done();
            }
        };

        var req = {
            body: { email: 'filipe@filipe.com'}
        };

        user.create(req, res);
    });

    it('Expected a failure when create user, cause missing "email" parameters', function(done) {

        var res = {
            status: function(statusCode) {
                this.statusCode = statusCode;
                return this;
            },
            send: function(err) {
                expect(err).to.be.a('object');
                expect(err).to.have.property('mensagem');
                expect(err.mensagem).to.equal('o campo email é obrigatório');
                expect(this.statusCode).to.be.a('number');
                expect(this.statusCode).to.equal(400);

                done();
            }
        };

        var req = {
            body: { nome: 'Filipe M. Silva'}
        };

        user.create(req, res);
    });

    it('Expected a failure when create user, cause missing "senha" parameter', function(done) {

        var res = {
            status: function(statusCode) {
                this.statusCode = statusCode;
                return this;
            },
            send: function(err) {
                expect(err).to.be.a('object');
                expect(err).to.have.property('mensagem');
                expect(err.mensagem).to.equal('o campo senha é obrigatório');
                expect(this.statusCode).to.be.a('number');
                expect(this.statusCode).to.equal(400);

                done();
            }
        };

        var req = {
            body: {
                nome: 'Filipe M. Silva',
                email: 'filipe@filipe.com'
            }
        };

        user.create(req, res);
    });

    it('Expected a failure when create user, cause missing "telefones" parameter', function(done) {

        var res = {
            status: function(statusCode) {
                this.statusCode = statusCode
                return this;
            },
            send: function(err) {

                expect(err).to.be.a('object');
                expect(err).to.have.property('mensagem');
                expect(err.mensagem).to.equal('o campo telefones é obrigatório');
                expect(this.statusCode).to.be.a('number');
                expect(this.statusCode).to.equal(400);

                done();
            }
        };

        var req = {
            body: {
                nome: 'Filipe M. Silva',
                email: 'filipe@filipe.com',
                senha: 'Senha123Forte'
            }
        };

        user.create(req, res);
    });

    it ('Should call create and receive a response', function(done) {

        var req = {}, res;

        req.body = {
            nome: 'Filipe',
            email:'filipe@filipe.com',
            senha: 'Senha123Forte',
            telefones: [{ numero: "123456789", ddd: "11" }]
        };

        res = {
            status(statusCode) {
                this.statusCode = statusCode;
                return this;
            },
            send(obj) {
                expect(obj).to.be.a('object');
                expect(obj).to.have.property('nome');
                expect(obj).to.have.property('email');
                expect(obj).to.have.property('senha');
                expect(obj).to.have.property('telefones');
                expect(obj).to.have.property('id');
                expect(obj).to.have.property('data_criacao');
                expect(obj).to.have.property('data_atualizacao');
                expect(obj).to.have.property('ultimo_login');
                expect(obj).to.have.property('token');
                expect(this.statusCode).to.equal(200);

                done();
            }
        };

        user.create(req, res);
    });

    it('Expected a failure when create user, cause missing "email" exist', function(done) {
        this.timeout(5000);
        var res = {
            status: function(statusCode) {
                this.statusCode = statusCode
                return this;
            },
            send: function(err) {
                expect(err).to.be.a('object');
                expect(err).to.have.property('mensagem');
                expect(err.mensagem).to.equal('email já existente');
                expect(this.statusCode).to.be.a('number');
                expect(this.statusCode).to.equal(409);
                done();
            }
        };

        var req = {
            body: {
                nome: 'Filipe M. Silva',
                email: 'filipe@filipe.com',
                senha: 'Senha123Forte',
                telefones: [{ numero: "123456789", ddd: "11" }]
            }
        };

        user.create(req, res);
    });

    after(function(done){
        var collection, db;

        db = DB({
            "username": "",
            "password": "#",
            "server": "localhost",
            "port": 27017,
            "database_name": "test-auth-user"
        });

        collection = db.collection('users');
        collection.remove({}).then(function() {
            done();
        }).catch(function(err) {
            done(err);
        });
    });
});
