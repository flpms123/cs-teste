'use strict';

const chai = require('chai');
const assert = require('assert');
const index = require('../../../model/user');
const expect = chai.expect;

describe('User index', function() {
    it ('Test index user object', function() {
        expect(index).to.be.a('object');
    });

    it('Test index has create function', function(){
        expect(index).to.have.property('create');
    });

    it('Test index has find function', function(){
        expect(index).to.have.property('find');
    });

    it('Test index has update function', function(){
        expect(index).to.have.property('update');
    });
});
