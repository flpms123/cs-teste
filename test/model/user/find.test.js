'use strict';

const chai = require('chai');
const assert = require('assert');
const DB = require('simple-connection');
const user = require('../../../model/user');
const expect = chai.expect;

describe('Find User', function() {

    var local = {};

    before(function(){
        global.environment = 'test';

        const uuid = require('node-uuid');
        const service = require('../../../service/');

        var collection, db, date, token, user;
        local.uuid = uuid.v4();
        date = new Date();

        user = {
            nome: 'Filipe',
            email: 'filipe@filipe.com',
            senha: 'Senha123Forte',
            telefones: [{ numero: "123456789", ddd: "11" }],
            _id: local.uuid,
            data_criacao: date,
            data_atualizacao: date,
            ultimo_login: date,
            senha: service.crypt('Senha123Forte')
        };

        token = service.token({email: user.email, nome: user.nome});
        user.token = service.crypt(token);

        db = DB({
            "username": "",
            "password": "#",
            "server": "localhost",
            "port": 27017,
            "database_name": "test-auth-user"
        });

        collection = db.collection('users');
        collection.insert(user).then(function(result) {
            done();
        }).catch(function(err) {
            done(err);
        });
    });

    it('expected failure cause don\'t send data', function(done) {
        user.find().catch((err) => {
            expect(err).to.be.a('object');
            expect(err).to.have.property('statusCode');
            expect(err).to.have.property('message');
            expect(err.statusCode).to.equal(400);
            expect(err.message).to.equal('sem dados para pesquisa');

            done();
        });
    });

    it('expected failure cause don\'t send valid data', function(done) {
        user.find('filipe@filipe.com').catch((err) => {
            expect(err).to.be.a('object');
            expect(err).to.have.property('statusCode');
            expect(err).to.have.property('message');
            expect(err.statusCode).to.equal(400);
            expect(err.message).to.equal('precisa ser um objeto para pesquisa');

            done();
        });
    });

    it('expected sucess in search with valid object and e-mail', function(done) {
        user.find({ email: 'filipe@filipe.com' }).then((result) => {

            expect(result[0]).to.have.property('nome');
            expect(result[0]).to.have.property('email');
            expect(result[0]).to.have.property('senha');
            expect(result[0].nome).to.equal('Filipe');
            expect(result[0].email).to.equal('filipe@filipe.com');

            done();
        });
    });

    it('expected sucess in search with id', function(done) {
        user.find({ _id: local.uuid}).then((result) => {

            expect(result[0]).to.have.property('nome');
            expect(result[0]).to.have.property('email');
            expect(result[0]).to.have.property('senha');
            expect(result[0].nome).to.equal('Filipe');
            expect(result[0].email).to.equal('filipe@filipe.com');
            expect(result[0]._id).to.equal(local.uuid);

            done();
        });
    });

    after(function(done) {
        var db, collection;

        db = DB({
            "username": "",
            "password": "#",
            "server": "localhost",
            "port": 27017,
            "database_name": "test-auth-user"
        });

        collection = db.collection('users');
        collection.remove({}).then(function() {
            done();
        }).catch(done);
    });
});
