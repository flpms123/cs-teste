'use strict';

const chai = require('chai');
const assert = require('assert');
const DB = require('simple-connection');

const user = require('../../../model/user/');
const expect = chai.expect;

describe('Model user test', function() {


    before(function(done) {

        global.environment = 'test';

        var collection, db;

        const uuid = require('node-uuid');
        const service = require('../../../service/');

        var collection, db, date, token, user;
        global.uuid = uuid.v4();
        date = new Date();

        user = {
            nome: 'Filipe',
            email: 'filipe@filipe.com',
            senha: 'Senha123Forte',
            telefones: [{ numero: "123456789", ddd: "11" }],
            _id: global.uuid,
            data_criacao: date,
            data_atualizacao: date,
            ultimo_login: date,
            senha: service.crypt('Senha123Forte')
        };

        token = service.token({email: user.email, senha: user.senha});
        user.token = service.crypt(token);

        db = DB({
            "username": "",
            "password": "#",
            "server": "localhost",
            "port": 27017,
            "database_name": "test-auth-user"
        });

        collection = db.collection('users');
        collection.insert(user).then(function(result) {
            done();
        }).catch(function(err) {
            done(err);
        });
    });

    it('Expected a failure when update user, cause user not informed', function(done) {
        user.update().catch(function(err) {
            expect(err).to.be.a('object');
            expect(err).to.have.property('statusCode');
            expect(err).to.have.property('message');
            expect(err.statusCode).to.equal(400);
            expect(err.message).to.equal('usuario nao informado');

            done();
        });
    });

    it('expect failure cause user email not defined', function(done) {
        user.update({
            $set: { nome: 'Filipe m. Silva'}
        }).catch(function(err) {
            expect(err).to.be.a('object');
            expect(err).to.have.property('statusCode');
            expect(err).to.have.property('message');
            expect(err.statusCode).to.equal(400);
            expect(err.message).to.equal('sem referencia para alteracao, informe o email ou id');

            done();
        });
    });

    it('expect failure cause user is informed without $set key', function(done) {
        user.update({ email: 'filipe@filipe.com', nome: 'Filipe m. Silva'}).catch(function(err) {
            expect(err).to.be.a('object');
            expect(err).to.have.property('statusCode');
            expect(err).to.have.property('message');
            expect(err.statusCode).to.equal(400);
            expect(err.message).to.equal('alteracao nao permitida');

            done();
        });
    });

    it('expect failure when update user token, cause email is undefined', function(done) {
        user.update({
            token: true,
            id: "8948f952-6516-4349-9374-37ef65f6d523",
            senha: 'Senha123Forte'
        }).catch(function(err) {
            expect(err).to.be.a('object');
            expect(err).to.have.property('statusCode');
            expect(err).to.have.property('message');
            expect(err.statusCode).to.equal(400);
            expect(err.message).to.equal('alteracao nao permitida email e/ou senha nao informado');

            done();
        });
    });

    it('expect failure when update user token, cause password is undefined', function(done) {
        user.update({
            token: true,
            email: 'filipe@filipe.com'
        }).catch(function(err) {

            expect(err).to.be.a('object');
            expect(err).to.have.property('statusCode');
            expect(err).to.have.property('message');
            expect(err.statusCode).to.equal(400);
            expect(err.message).to.equal('alteracao nao permitida email e/ou senha nao informado');

            done();
        });
    });

    it('expect sucess when update user token', function(done) {
        user.update({
            token: true,
            email: 'filipe@filipe.com',
            senha: 'Senha123Forte'
        }).then(function(result) {
            expect(result).to.be.a('object');
            expect(result).to.have.property('ok');
            expect(result).to.have.property('nModified');
            expect(result).to.have.property('n');
            expect(result.ok).to.equal(1);
            expect(result.nModified).to.equal(1);
            expect(result.n).to.equal(1);

            done();
        }).catch(done) ;
    });

    it('expect sucess when update password and use id', function(done) {

        user.update({
            id: global.uuid,
            $set :{senha: 'Senha1234Forte'}
        }).then(function(result) {
            expect(result).to.be.a('object');
            expect(result).to.have.property('ok');
            expect(result).to.have.property('nModified');
            expect(result).to.have.property('n');
            expect(result.ok).to.equal(1);
            expect(result.nModified).to.equal(1);
            expect(result.n).to.equal(1);

            done();
        }).catch(done) ;
    });

    after(function(done) {
        var db, collection;

        db = DB({
            "username": "",
            "password": "#",
            "server": "localhost",
            "port": 27017,
            "database_name": "test-auth-user"
        });

        collection = db.collection('users');
        collection.remove({}).then(function() {
            done();
        }).catch(done);
    });
});
