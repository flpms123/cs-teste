'use strict';

const chai = require('chai');
const assert = require('assert');
const user = require('../../../model/user');

const expect = chai.expect;

describe('Creating User', function() {

    before(function(){
        global.environment = 'test';
    });

    it('Expected a failure when create user, cause "telefones.ddd" parameter as wrong', function(done) {
        user.create({
            nome: 'Filipe',
            email:'filipemelodasilva@gmail.com',
            senha: 'Senha123Forte',
            telefones: [ { ddd: 'aa'} ]
        }).catch(function(err) {

            expect(err).to.be.a('object');
            expect(err).to.have.property('statusCode');
            expect(err).to.have.property('message');
            expect(err.statusCode).to.equal(400);
            expect(err.message).to.equal('o campo telefones precisa ter um numero com ddd valido');
            done();
        })
    });

    it('Expected a failure when create user, cause "telefones.ddd" was missing', function(done) {
        user.create({
            nome: 'Filipe',
            email:'filipemelodasilva@gmail.com',
            senha: 'Senha123Forte',
            telefones: [ { numero: '123456789'} ]
        }).catch(function(err) {

            expect(err).to.be.a('object');
            expect(err).to.have.property('statusCode');
            expect(err).to.have.property('message');
            expect(err.statusCode).to.equal(400);
            expect(err.message).to.equal('o campo telefones precisa ter um numero com ddd valido');
            done();
        });
    });

    it('Expected a failure when create user, cause "telefones.numero" was missing', function(done) {
        user.create({
            nome: 'Filipe',
            email:'filipemelodasilva@gmail.com',
            senha: 'Senha123Forte',
            telefones: [ { ddd: '11'} ]
        }).catch(function(err) {

            expect(err).to.be.a('object');
            expect(err).to.have.property('statusCode');
            expect(err).to.have.property('message');
            expect(err.statusCode).to.equal(400);
            expect(err.message).to.equal('o campo telefones precisa ter um numero com ddd valido');

            done();
        });
    });

    it('Expected a failure when create user, cause "telefones.numero" was wrong', function(done) {
        user.create({
            nome: 'Filipe',
            email:'filipemelodasilva@gmail.com',
            senha: 'Senha123Forte',
            telefones: [ { ddd: '11', numero: '12345'} ]
        }).catch(function(err) {

            expect(err).to.be.a('object');
            expect(err).to.have.property('statusCode');
            expect(err).to.have.property('message');
            expect(err.statusCode).to.equal(400);
            expect(err.message).to.equal('o campo telefones precisa ter um numero com ddd valido');

            done();
        });
    });

    it('Expected a sucess when create user', function(done) {
        user.create({
            nome: 'Filipe',
            email:'filipemelodasilva@gmail.com',
            senha: 'Senha123Forte',
            telefones: [{ numero: "123456789", ddd: "11" }]
        }).then(function(result) {

            expect(result).to.be.a('object');
            expect(result).to.have.property('result');
            expect(result).to.have.property('ops');
            expect(result.result.ok).to.equal(1);
            expect(result.result.n).to.equal(1);

            done();
        });
    });
});
