'use strict';

const chai = require('chai');
const assert = require('assert');
const service = require('../../service/');
const expect = chai.expect;

describe('User index', function() {
    it('failure cause token undefined', function(done){

        var req, res;

        req = {
            headers: {}
        };

        res = {
            status(statusCode){
                this.statusCode = statusCode;
                return this;
            },
            send(result) {
                expect(result).to.be.a('object');
                expect(result).to.have.property('mensagem');
                expect(result.mensagem).to.equal('Não autorizado');

                expect(this.statusCode).to.be.a('number');
                expect(this.statusCode).to.equal(401);

                done();
            }
        }

        service.validateToken(req, res);
    });
});
