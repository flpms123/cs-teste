'use strict';

const chai = require('chai');
const assert = require('assert');
const service = require('../../service/');
const expect = chai.expect;

describe('User index', function() {
    it ('Test index user object', function() {
        expect(service).to.be.a('object');
    });

    it('Test index has crypt function', function(){
        expect(service).to.have.property('crypt');
    });

    it('Test index has token function', function(){
        expect(service).to.have.property('token');
    });

    it('Test index has validate-token function', function(){
        expect(service).to.have.property('validateToken');
    });
});
