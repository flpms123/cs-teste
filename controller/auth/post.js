'use strict';

const User = require('../../model/user/');
const service = require('../../service/');

let create = function(req, res) {

    let fail = err => {
        if (err.statusCode) {
            res.status(err.statusCode).send({ mensagem: err.message });
        } else {
            res.status(503).send({});
        }
    };

    if (!req.body) {
        return fail({ statusCode: 400, message: 'solicitacao invalida' });
    }

    if (!req.body.email || !req.body.senha) {
        return fail({ statusCode: 400, message: 'usuario e/ou senha nao informado' });
    }

    let email = req.body.email;
    let senha = req.body.senha;

    let user = User.find({ email: email});

    user.then(result => {

        if (!result.length) {
            return fail({statusCode: 401, message: 'Usuário e/ou senha inválidos'});
        }

        let cryptedPass = service.crypt(senha);

        if (cryptedPass !== result[0].senha) {
            return fail({statusCode: 401, message: 'Usuário e/ou senha inválidos'});
        }

        let date = new Date();

        return User.update({/* eslint-disable */
            id: result[0]._id,
            $set: {
                data_atualizacao: date,
                ultimo_login: date
            }/* eslint-disable */
        }).then(resultUpdate => {

            if (resultUpdate.ok !== 1) {
                fail({statusCode: 503, message: 'something wrong in login service'});
                return;
            }
            /* eslint-disable */
            result[0].id = result[0]._id;
            result[0].data_atualizacao = date;
            result[0].ultimo_login = date;
            delete result[0].senha;
            delete result[0]._id;
            /* eslint-disable */

            res.status(200).send(result[0]);
        }).catch(err => fail(err));
    });

    user.catch(fail);

    return false;
};

module.exports = create;
