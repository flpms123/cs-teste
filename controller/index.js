'use strict';

const User = require('./user');
const Auth = require('./auth');

module.exports = {
    user: User,
    auth: Auth
}
