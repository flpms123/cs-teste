'use strict';

const post = require('./post.js');
const get = require('./get.js');

module.exports = {
    create: post,
    find: get
};
