'use strict';

const moment = require('moment');
const User = require('../../model/user');

let user = function(req, res) {

    let fail = err => {
        if (err.statusCode) {
            res.status(err.statusCode).send({ mensagem: err.message });
        } else {
            res.status(503).send({});
        }
    };

    if (!req.params.id) {
        return fail({statusCode: 400, message: 'usuario nao informado'});
    }

    return User.find({ _id: req.params.id }).then(result => {

        if (!Object.keys(result).length) {
            return fail({statusCode: 404, message: 'usuario nao encontrado'});
        }

        let token = req.token.replace((/Bearer\u0020/gi), '');

        if (token !== result[0].token) {
            return fail({statusCode: 401, message: 'Não autorizado'});
        }

        let loginTime = moment(result[0].ultimo_login).add(30, 'm');

        if (moment().isAfter(loginTime)) {
            return fail({statucCode: 401, mensagem: 'Sessão inválida'});
        }

        result.id = result[0]['_id'];

        /*eslint-disable*/
        delete result[0].senha;
        delete result[0]['_id'];
        /*eslint-disable*/

        return res.status(200).send(result[0]);

    }).catch(fail);
};

module.exports = user
