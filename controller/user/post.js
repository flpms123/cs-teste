'use strict';

const User = require('../../model/user');

let create = function(req, res) {

    let fail = err => {
        if (err.statusCode) {
            res.status(err.statusCode).send({mensagem: err.message});
        } else {
            res.status(503).send({});
        }
    };

    if (!req.body || !Object.keys(req.body).length) {
        return fail({ statusCode: 400, message: 'nenhum usuario informado' });
    }

    let user = req.body;

    if (!user.nome) {
        return fail({statusCode: 400, message: 'o campo nome é obrigatório' });
    }

    if (!user.email) {
        return fail({ statusCode: 400, message: 'o campo email é obrigatório' });
    }

    if (!user.senha) {
        return fail({statusCode: 400, message: 'o campo senha é obrigatório' });
    }

    if (!user.telefones || !user.telefones.length) {
        return fail({statusCode: 400, message: 'o campo telefones é obrigatório' });
    }

    return User.find({
        email: user.email
    }).then(result => {

        if (typeof result[0] !== 'undefined' && result.length > 0 && result[0].email === user.email) {

            /* eslint-disable */
            throw { statusCode: 409, message: 'email já existente'};
            /* eslint-disable */
            return;
        }

        User.create(req.body).then(result => {

            let obj = result.ops.map(op => {

                op.id = op['_id'];
                delete op['_id'];

                return op;
            });

            res.status(200).send(obj[0]);

        }).catch(err => fail(err));

    }).catch(err => fail(err));
};

module.exports = create;
