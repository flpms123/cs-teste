'use strict';

const router = require('express').Router();
const controller = require('../controller');
const service = require('../service');

const User = controller.user;
const Auth = controller.auth;

router.post('/sign_up', User.create);
router.post('/sign_in', Auth.create);
router.get('/user/:id', service.validateToken, User.find);

router.all('/', (req, res, next) => {
    console.log('Work fine in ' + environment);
    res.status(200).send({message: 'working'});
});

module.exports = router;
