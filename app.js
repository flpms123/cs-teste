'use strict';

let express = require('express');
let bodyParser = require('body-parser');
let morgan = require('morgan');
let routesApi = require('./routes/api.js');

let app = express();
let args = process.argv;
let passedPort = args[2];
let environment = args[3];

if (environment !== 'prod' && environment !== 'dev' && environment !== 'homolog') {
    throw new Error('Environment is not defined. Follow sintax: \n > node index.js |port| [dev|homolog|prod]');
}

if (isNaN(passedPort)) {
    throw new Error('Port is not a number. \n Follow sintax: \n > node index.js |port| |log-options|');
}

app.port = passedPort || 3000;
app.use(environment === 'prod' ? morgan('common') : morgan('dev'));

global.environment = environment;

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, PUT, PATCH, POST, DELETE');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    return req.method === 'OPTIONS' ? res.status(204).send() : next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/api', routesApi);

app.use((req, res, next) => {
    let err = new Error();

    err.status = 404;
    next(err);
    return;
});

app.use((err, req, res) => {
    res.status(err.status || 500).send(err.message);
    return;
});

module.exports = app;
